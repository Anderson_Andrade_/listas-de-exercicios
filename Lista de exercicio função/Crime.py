nome = input("Digite seu nome: ")

def crime (nome):
    respostas = 0
    for i in range(1):

        pergunta_1 = input("Telefonou para a vitima? [n/y]").upper()

        if pergunta_1 == "Y":
            respostas += 1

        pergunta_2 = input("Esteve no local do crime ? [n/y]").upper()

        if pergunta_2 == "Y":
            respostas += 1

        pergunta_3 = input("Mora perto da vitima ? [n/y]").upper()

        if pergunta_3 == "Y":
            respostas += 1

        pergunta_4 = input("Devia para a vitima ? [n/y]").upper()

        if pergunta_4 == "Y":
            respostas += 1

        pergunta_5 = input("Já trabalhou com a vitima ? [n/y]").upper()

        if pergunta_5 == "Y":
            respostas += 1

    if respostas < 2:
        print("O suspeito {} é INOCENTE" .format(nome))

    elif respostas == 2:
        print("O suspeito {} é SUSPEITO".format(nome))

    elif respostas == 3 or respostas == 4:
        print("O suspeito {} é CUMPLICE".format(nome))

    elif respostas == 5:
        print("O suspeito {} é ASSASSINO".format(nome))

crime(nome)