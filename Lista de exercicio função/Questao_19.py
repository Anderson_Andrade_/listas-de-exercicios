import random

print("*" * 50)


servidores = ["Windows","Unix","Linux","NetWare","Mac","Outros"]
votos_servidores = [0,0,0,0,0,0]

windows = 0
unix = 0
linux = 0
net = 0
mac = 0
outros = 0
cont = 0

while (True):
    #
    # resposta = int(input('''Qual o melhor SO para uso em servidores?
    # Windows[1]
    # Unix[2]
    # Linux[3]
    # Net_wer[4]
    # Mac[5]
    # outros[6]
    # Sair da aplicação[0]'''))

    resposta = random.randint(0,7)

    if (resposta == 1):
        windows += 1
        votos_servidores[0] = windows
        cont +=1

    elif (resposta == 2):
        unix += 1
        votos_servidores[1] = unix
        cont +=1

    elif (resposta == 3):
        linux += 1
        votos_servidores[2] = linux
        cont +=1

    elif (resposta == 4):
        net += 1
        votos_servidores[3] = net
        cont +=1

    elif (resposta == 5):
        mac += 1
        votos_servidores[4] = mac
        cont +=1

    elif (resposta == 6):
        outros += 1
        votos_servidores[5] = outros
        cont +=1

    elif (resposta == 0):
        print("-"*51)
        print("Sistemas Operacionais             Votos        %")
        print("-" * 51)
        print("Windows                           {}           {:.2f}" .format(windows, (windows * 100 /cont)))
        print("Unix                              {}           {:.2f}" .format(unix, (unix * 100 / cont)))
        print("Linux                             {}           {:.2f}" .format(linux, (linux * 100 / cont)))
        print("Net                               {}           {:.2f}" .format(net, (net * 100 / cont)))
        print("Mac                               {}           {:.2f}" .format(mac, (mac * 100 / cont)))
        print("Outros                            {}           {:.2f}" .format(outros, (outros * 100 / cont)))
        print("-" * 51)
        print("Total                             {}" .format(cont))
        print("-" * 51)
        max_vot = 0
        for i in range(len(votos_servidores)):

            if votos_servidores[i] > max_vot:
                max_so = servidores[i]
                max_vot = votos_servidores[i]
        print("Vencedor: {} com {} votos" .format(max_so, max_vot))
        print("-" * 51)
        print("Create by:\nAnderson Andrade(Debug)\nRômulo Cavalcante")
        break

    else:
        print("Resposta invalida, sua resposta {}".format(resposta) )