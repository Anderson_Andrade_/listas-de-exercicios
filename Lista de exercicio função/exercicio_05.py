# Create date: 28 agust 2018
# Create by: Anderson Andrade (Debug) / Romulo Cavalcante

valor_bruto = float(input("Digite o valor bruto do produto: "))

taxa_imposto = float(input("Digite o valor da porcentagem do imposto: "))

def calculo_imposto(valor_bruto, taxa_imposto):
    custo = ((valor_bruto * taxa_imposto) /100 + valor_bruto)
    return custo

print('''
O valor bruto é de {}
A taxa de imposto é de {}
O custo total é de {}
'''.format(valor_bruto,taxa_imposto,calculo_imposto(valor_bruto,taxa_imposto)))