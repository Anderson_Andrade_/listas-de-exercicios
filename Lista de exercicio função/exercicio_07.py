import random

number_of_days = random.randint(0, 30)
total_paid = 0
provision = []


def payment_amount(values, number_of_days, interest, traffic_ticket):
    if number_of_days == 0 and number_of_days < 0:
        provision.append(values)
    else:
        values = ((values * traffic_ticket) / 100) + values + ((number_of_days * interest) / 100)
        provision.append(values)


while True:

    interest = 0.1
    traffic_ticket = 3

    total_paid = sum(provision)

    values = random.randint(0, 30)
    total_of_days = len(provision)

    if values == 0:
        print("Total of days {:.2f} \namounts pays {}".format(total_of_days, total_paid))
        print("-" * 50)
        print("Create by: Anderson Andrade (Debug) and Romulo Cavalcante\nDate: 9/4/2018")
        break

    payment_amount(values, number_of_days, interest, traffic_ticket)