import random

impar = []
par = []
geral = []

for i in range (20):
    valor = random.randint(1,30)
    geral.append(valor)

for j in range (20):
    if geral[j]%2 == 1:
        impar.append(geral[j])
    else:
        par.append(geral[j])

print("Todos os números: ",geral)
print("Números impares: ", impar)
print("Números pares: ", par)